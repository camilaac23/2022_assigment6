#INTEGRANTES GRUPO 2:
#CAMILA ACHICANOY CAICEDO
#EVER MAURICIO RODRIGUEZ
#GLORIA DANIELA TIMARÁN

#!/usr/bin/python
# -*- coding: utf-8 -*-
import socket #utilidades de red y conexion
#declaramos las variables
ipServidor = "192.168.97.22" #es lo mismo que "localhost" o "0.0.0.0"
puertoServidor = 9798

#socket.AF_INET para indicar que utilizaremos Ipv4
#socket.SOCK_STREAM para utilizar TCP/IP (no udp)
#Estos protocolos deben ser los mismos que en el servidor
cliente = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
cliente.connect((ipServidor, puertoServidor))
print("Conectado con el servidor ---> %s:%s" %(ipServidor, puertoServidor))

while True:

    msg = input("Escriba la ecuación cuadrática de la forma ax^2+bx+c : ")
    cliente.send(msg.encode('UTF-8'))

    respuesta = cliente.recv(4096)
    print('Respuesta {}'.format(respuesta.decode('utf-8')))
    if respuesta == "exit":
        break;

print("------- CONEXIÓN CERRADA ---------")
cliente.close()
