#INTEGRANTES GRUPO 2:
#CAMILA ACHICANOY CAICEDO
#EVER MAURICIO RODRIGUEZ
#GLORIA DANIELA TIMAR�N

import numpy as np
import re
import socket
import threading

host = ""
port = 9798

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print ("Socket Created")
sock.bind((host, port))
print ("socket bind complete")
sock.listen(1)
print ("socket now listening")

def worker(*args):
    conn = args[0]
    addr = args[1]
    try:
        print('conexion con {}.'.format(addr))

        while True:
            datos = conn.recv(4096)
            if datos:

                s = datos.decode('UTF-8')
                re_ecuacion_cuadratica = re.compile(r'(-?\d+)x\^2([+-]\d+)x([+-]\d+)')
                ecuacion_cuadratica = s
                matches = re_ecuacion_cuadratica.match(ecuacion_cuadratica)
                a = int(matches.group(1))
                b = int(matches.group(2))
                c = int(matches.group(3))
                list = [a, b, c]
                raices = np.roots(list)
                #print('X1=', raices[0], 'X2=', raices[1])
                stor = 'X1=', raices[0], 'X2=', raices[1]
                stor2 = ' '.join(map(str, stor))
                print('recibido: {}'.format(datos.decode('UTF-8')))
                conn.send(stor2.encode('UTF-8'))

            else:
                print("prueba")
                break
    finally:
        print("close")
        conn.close()

while 1:
    conn, addr = sock.accept()
    threading.Thread(target=worker, args=(conn, addr)).start()
